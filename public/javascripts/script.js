var $bat = $("#hunter");
var $ball = $("#ball");
var $aliens = $("#tweets");
var $scoreboard = $("#score span");
var $toKillboard = $('#tokill span');
var $trollBird = $('#trollbird');
var emoticons = $('#emoticons img');
var icons = [];
var emoticonsHeight = $('#emoticons').outerHeight();
var birdSpeed = 7;
var ww = $('#gameContainer').width();
var wh = $('#gameContainer').height();
var ballX = 0;
var ballY = -1;
var tick = 2;
var newLeft = 0;
var trollBirdWidth = $trollBird.outerWidth();
var aliens = [];
var FRAMERATE = 1000 / 60;
var score = 0;
var alienNr = $aliens.children().length;
$toKillboard.text(alienNr);

$bat.css({
	'top' : wh - $bat.height(),
});

function rand(min, max) {
	return Math.round(Math.random() * (max - min)) + min;
}

function pos($el) {
	return {
		width : parseInt($el.outerWidth()),
		height : parseInt($el.outerHeight()),
		x : parseInt($el.css('left')),
		y : parseInt($el.css('top'))
	};
}

function isCollide(b) {
	// ball
	var a = {
		x : ballX,
		y : ballY,
		width : 30,
		height: 30
	};

	return !(
		((a.y + a.height) < (b.y)) ||
		(a.y > (b.y + b.height)) ||
		((a.x + a.width) < b.x) ||
		(a.x > (b.x + b.width))
	);
}

function moveAliens() {
	aliens = aliens.map(function(alien) {
		alien.y += tick;
		if ((alien.y - alien.height) > wh && alien.hit == false) {
			alien.y = alien.startOffset;
			alien.addClass('pooping');
			alien.ignore = false;
		}
		
		if(alien.y > 40 && alien.hasClass('pooping')){
			alien.x = $trollBird.offset().left;
			alien.css('left', alien.x);
			alien.removeClass('pooping');
		}
		if(alien.y > (wh - emoticonsHeight) - alien.height && alien.ignore == false) {
			icons.map(function(icon) {
				if ((icon.x + icon.width) > alien.x && icon.x < (alien.x + alien.width) ){
					if (icon.state == 'happy'){
						icon.attr('src', 'https://twemoji.maxcdn.com/72x72/1f61f.png');
						icon.state = 'confused';
						alien.ignore = true;
					}
					else if (icon.state == 'confused'){
						icon.attr('src', 'https://twemoji.maxcdn.com/72x72/1f621.png');
						icon.state = 'angry';
						alien.ignore = true;
					}
					else if (icon.state == 'angry'){
						icon.attr('src', 'https://twemoji.maxcdn.com/72x72/1f635.png');
						icon.state = 'done';
						alien.ignore = true;
					}
					//alien.remove();
				}
			});
		}
		
		if (isCollide(alien) && !alien.hit) {
			ballY = -1;
			alien.hit = true;
			score += alien.followers;
			$scoreboard.text(score);
			alienNr -= 1;
			$toKillboard.text(alienNr);
			$alien.ignore = true;
			
			if (alienNr == 0){
				youDidIt();
			}
		}
		
		return alien;
	});
}

function drawAliens() {
	aliens.forEach(function(alien, index) {
		var $alien = $aliens.find("article").eq(index);
	//	console.log($alien);
		$alien.css({
			left : alien.x,
			top : alien.y
		});

		if (alien.hit) {
			$alien.hide();
			if (alien.firstHit = false){
				var audio = new Audio('/sounds/chat.mp3');
				audio.play();
				alien.firstHit = true;
			}
			
			$alien.first = false;
			$alien.ignore = true;
			
		}
	});
}

function initAliens() {
	var i = 0;
	$('.tweet').each(function() {
		$alien = $(this);
		$alien.hit = false;
		$alien.firstHit = false;
		$alien.width = $(this).outerWidth();
		$alien.height = $(this).outerHeight();
		$alien.x = rand(0, (ww - $(this).outerWidth()));
		$alien.y = -($(this).outerHeight() * 2) * i;
		$alien.startOffset = -($(this).outerHeight() * 4) * i;
		$alien.followers = $(this).data('followers');
		$alien.ignore = false;
		//$(this).css({'margin-top': -(($(this).outerHeight() * 2) * i)});
		aliens.push($alien);
		i++;
	});
	
}

function drawBall() {
	if (ballY === -1) {
		$ball.hide();
		return;
	} else {
		$ball.show();
	}

	ballY -= birdSpeed;

	if (ballY < 0) {
		ballY = -1;
	}

	$ball.css({
		'top' : ballY,
		'left' : ballX + 40
	});
}

function initBat() {
	$(window).on('mousemove', function(e) {
		$bat.css('left', e.clientX + 'px');
	});

	$(window).on('click', function(e) {
		ballX = e.clientX;
		ballY = wh;
	});
}
function trollBird() {
	if ((newLeft + birdSpeed + trollBirdWidth) > ww || $trollBird.hasClass('flip')) {
		newLeft -= birdSpeed;
		$trollBird.addClass('flip');
		if ((newLeft + birdSpeed) < 0){
			$trollBird.removeClass('flip');
		}
	} else {
		newLeft += birdSpeed;
	}
	
	$trollBird.css('left', newLeft);
}

function emoticonsHurt() {
	emoticons.each(function() {
		$icon = $(this);
		$icon.x = $(this).offset().left;
		$icon.y = $(this).offset().top;
		$icon.width = $(this).width();
		$icon.state = 'happy';
		icons.push($icon);
	});
}

function youDidIt() {
	$('<div id="youDidIt"><h1>YOU MADE IT!</h1><a href="#">Want to report the trollers?</a></div>').appendTo('#gameContainer');
}
function setup() {
	initBat();
	initAliens();
	emoticonsHurt();
	requestAnimationFrame(draw);
}

function draw() {
	drawBall();

	moveAliens();
	drawAliens();
	trollBird();
	
	setTimeout(function() {
		requestAnimationFrame(draw);
	}, FRAMERATE);
}
setup();


window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
 
  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };
 
  return t;
}(document, "script", "twitter-wjs"));